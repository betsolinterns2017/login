from django.contrib import admin
from .models import DeploymentInstance, Script, SlackBot


class DeploymentAdmin(admin.ModelAdmin):
    list_display = ('name', 'description', 'codeUrl')
    search_fields = ['name']


class ScriptAdmin(admin.ModelAdmin):
    list_display = ('name', 'type', 'orderNo')
    search_fields = ['name']


class SlackAdmin(admin.ModelAdmin):
    list_display = ('name', 'status', 'channelNo')


admin.site.register(DeploymentInstance, DeploymentAdmin)
admin.site.register(Script, ScriptAdmin)
admin.site.register(SlackBot, SlackAdmin)
