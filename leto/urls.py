from django.conf.urls import url, include
from django.contrib import admin
from . import views

urlpatterns = [
    url(r'^$', views.login_user, name="login_user"),
    url(r'^dashboard/', views.dashboard, name="dashboard"),
    url(r'^logout/',views.logout_user, name="logout_user"),
    url(r'^password/$', views.change_password, name='change_password'),
    url(r'^deploymentinstance/', include('deploymentinstance.urls')),
]