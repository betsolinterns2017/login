from django.utils.encoding import python_2_unicode_compatible

from django.db import models


class DeploymentInstance(models.Model):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=100)
    codeUrl = models.CharField(max_length=100)
    STATUS = (
        ('R', 'Running'),
        ('S', 'Stopped'),
    )
    status = models.CharField(max_length=1, choices=STATUS, editable=False, default='R')


    def __str__(self):
        return self.name

class Script(models.Model):
    name = models.CharField(max_length=100)
    SCRIPT_TYPE = (
        ('P', 'Python'),
        ('B', 'Batch'),
        ('S', 'Shell'),
    )
    type = models.CharField(max_length=1, choices=SCRIPT_TYPE)
    scriptText = models.TextField()
    orderNo = models.IntegerField(unique=True, help_text='Order of Execution')
    instanceId = models.ForeignKey(
        DeploymentInstance,
        on_delete=models.CASCADE
    )

    def __str__(self):
        return self.name


class SlackBot(models.Model):
    name = models.CharField(max_length=100)
    STATUS = (
        ('R', 'Running'),
        ('S', 'Stopped'),
    )
    status = models.CharField(max_length=1, choices=STATUS, editable=False)
    channelNo = models.IntegerField(default=1)
    key = models.IntegerField(default=100)
    botToken = models.CharField(max_length=100)

