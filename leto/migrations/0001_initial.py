# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-02-16 18:31
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='DeploymentInstance',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('description', models.CharField(max_length=100)),
                ('codeUrl', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='Script',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('type', models.CharField(choices=[('P', 'Python'), ('B', 'Batch'), ('S', 'Shell')], max_length=1)),
                ('scriptText', models.TextField()),
                ('orderNo', models.IntegerField(help_text='Order of Execution', unique=True)),
                ('instanceId', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='leto.DeploymentInstance')),
            ],
        ),
        migrations.CreateModel(
            name='SlackBot',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('status', models.CharField(choices=[('R', 'Running'), ('S', 'Stopped')], editable=False, max_length=1)),
                ('channelNo', models.IntegerField(default=1)),
                ('key', models.IntegerField(default=100)),
                ('botToken', models.CharField(max_length=100)),
            ],
        ),
    ]
