from django.http import HttpResponse
from django.http import *
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.forms import PasswordChangeForm
from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from leto.models import DeploymentInstance

# Create your views here.
def login_user(request):
    logout(request)
    username = password = ''
    if request.POST:
        username = request.POST['username']
        password = request.POST['password']

        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                return HttpResponseRedirect('/leto/dashboard/')
    return render(request, 'login.html')

@login_required(login_url='/leto/')
def dashboard(request):
    deployinstcount = DeploymentInstance.objects.all().count()
    context = {'deployinstcount': deployinstcount}
    return render(request,'dashboard.html', context)

@login_required(login_url='/leto/')
def logout_user(request):
    logout(request)
    return HttpResponseRedirect('/leto/')

@login_required(login_url='/leto/')
def change_password(request):
    if request.POST:
        oldpassword = request.POST['oldpassword']
        newpassword = request.POST['newpassword']
        repassword = request.POST['repassword']
        if oldpassword == "" or newpassword == "" or repassword == "":
            messages.error(request, "Please enter all the fields!")
            return redirect('change_password')
        user = User.objects.get(id = request.user.id)
        if user.check_password(oldpassword):
            if newpassword==repassword:
                if (len(newpassword)>8) and (sum(c.isdigit() for c in newpassword)>2) and (any(c.isupper() for c in newpassword)) and newpassword!=oldpassword:
                    user.set_password(newpassword)
                    user.save()
                    update_session_auth_hash(request, user)
                    messages.success(request, 'Your password was successfully updated!')
                    return redirect('dashboard')
                else:
                    messages.error(request, 'Format of password is incorrect')
                    return redirect('change_password')
            else:
                messages.error(request, 'New password and confirm password fields do not match!')
                return redirect('change_password')
        else:
            messages.error(request, 'Current password enterred is incorrect')
            return redirect('change_password')
    return render(request, 'change_password.html')



"""@login_required(login_url='/leto/')
def change_password(request):
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Important!
            messages.success(request, 'Your password was successfully updated!')
            return redirect('dashboard')
        else:
            messages.error(request, 'Please correct the error below.')
    else:
        form = PasswordChangeForm(request.user)
    return render(request, 'change_password.html', {
        'form': form
    })"""