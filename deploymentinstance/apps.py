from django.apps import AppConfig


class DeploymentinstanceConfig(AppConfig):
    name = 'deploymentinstance'
