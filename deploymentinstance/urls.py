from django.conf.urls import url, include
from django.contrib import admin
from . import views

urlpatterns = [
    url(r'^$', views.deploymentinstance, name="deploymentinstance"),
    url(r'^add/',views.add, name = "add_deploymentinstance"),
    url(r'^delete/(?P<instance_id>[0-9]+)/$', views.delete, name= "delete_deploymentinstance"),
    url(r'^edit/(?P<instance_id>[0-9]+)/$', views.edit, name= "edit_deploymentinstance"),

]