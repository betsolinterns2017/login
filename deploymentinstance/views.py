from django.http import HttpResponse
from django.http import *
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.forms import PasswordChangeForm
from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from leto.models import DeploymentInstance

@login_required(login_url='/leto/')
def deploymentinstance(request):
    all_deployments = DeploymentInstance.objects.all()
    context = {'all_deployments': all_deployments}
    return render(request, 'deploymentinstance.html', context)

@login_required(login_url='/leto/')
def add(request):
    #Check for proper conditions before enterring into the database (example blank fields)
    if request.POST:
        di = DeploymentInstance()
        di.name = request.POST['name']
        di.description = request.POST['description']
        di.codeUrl = request.POST['codeurl']
        di.status = 'R'
        di.save()
        return redirect('deploymentinstance')
    return render(request, 'add_deploymentinstance.html')

@login_required(login_url='/leto')
def delete(request, instance_id):
    inst = DeploymentInstance.objects.get(id=instance_id)
    inst.delete()
    return redirect('deploymentinstance')

@login_required(login_url='/leto')
def edit(request, instance_id):
    inst = DeploymentInstance.objects.get(id=instance_id)
    if request.POST:
        if request.POST['name']!='':
            inst.name = request.POST['name']
        if request.POST['description']!='':
            inst.description = request.POST['description']
        if request.POST['codeurl']!='':
            inst.codeUrl = request.POST['codeurl']
        inst.save()
        messages.success(request, 'The database was successfully updated!')
        return redirect('deploymentinstance')
    else:
        context = {'inst': inst}
        messages.info(request, 'Leave the field blank to retain old data')
        return render(request, 'edit_deploymentinstance.html', context)